package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/16 18:11
 * description: 供应商
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;


    /**
     * 分页查询供应商
     * @param page  页码
     * @param rows  每页显示的记录数
     * @param supplierName  供应商名称
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getSupplierList(Integer page,Integer rows,String supplierName){
        return supplierService.getSupplierList(page,rows,supplierName);
    }

    /**
     * 保存或供应商信息
     * @param supplier
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveSupplier(Supplier supplier){
        return supplierService.saveSupplier(supplier);
    }

    /**
     * 删除供应商
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String ids){
        return supplierService.deleteSupplier(ids);
    }
}
