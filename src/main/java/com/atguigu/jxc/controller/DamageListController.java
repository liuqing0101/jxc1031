package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/19 8:45
 * description:商品报损
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageListController {
    @Autowired
    private DamageListService damageListService;

    /**
     * 保存报损单
     * @param session（会话） 用户登录的时候我们就需要把用户信息保存至session中，可以从session中获取用户信息
     * @param damageList  报损单
     * @param damageListGoodsStr  报损商品单
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveDamageListGoods(HttpSession session, DamageList damageList, String damageListGoodsStr){
        return damageListService.saveDamageListGoods(session,damageList,damageListGoodsStr);
    }


    /**
     * 报损单查询
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getDamageList(HttpSession session,String sTime,String eTime){
        return damageListService.getDamageList(session,sTime,eTime);
    }
}
