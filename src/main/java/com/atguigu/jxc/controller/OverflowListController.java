package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author liuqing
 * @date 2023/5/19 16:58
 * description:
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListController {
    @Autowired
    private OverflowListService overflowListService;

    @PostMapping("/save")
    public ServiceVO saveOverflowList(HttpSession session,OverflowList overflowList, String overflowListGoodsStr){
        return overflowListService.saveOverflowList(session,overflowList,overflowListGoodsStr);

    }
}
