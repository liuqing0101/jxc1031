package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/16 21:23
 * description:
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    /**
     * 分页展示客户
     * @param page 当前页码
     * @param rows 每页显示记录数
     * @param customerName 客户名字
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getCustomerList(Integer page,Integer rows,String customerName) {
        return customerService.getCustomerList(page,rows,customerName);
    }


    /**
     * 保存或者修改客户
     * @param customer 客户
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveCustomer(Customer customer){
        return customerService.saveCustomer(customer);
    }

    @PostMapping("/delete")
    public ServiceVO deleteCustomer(String ids){
        return customerService.deleteCustomer(ids);
    }

}
