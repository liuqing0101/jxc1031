package com.atguigu.jxc.service;

import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/17 11:45
 * description:
 */
public interface UnitService {
    Map<String, Object> getUnitList();
}
