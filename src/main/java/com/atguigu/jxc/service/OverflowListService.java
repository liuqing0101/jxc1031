package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;

/**
 * @author liuqing
 * @date 2023/5/19 17:00
 * description:
 */
public interface OverflowListService {
    ServiceVO saveOverflowList(HttpSession session,OverflowList overflowList, String overflowListGoodsStr);
}
