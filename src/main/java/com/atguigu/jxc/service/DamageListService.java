package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/19 8:48
 * description:
 */
public interface DamageListService {
    ServiceVO saveDamageListGoods(HttpSession session, DamageList damageList, String damageListGoodsStr);

    Map<String, Object> getDamageList(HttpSession session, String sTime, String eTime);
}
