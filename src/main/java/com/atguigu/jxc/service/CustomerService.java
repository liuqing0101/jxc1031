package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/16 21:27
 * description:
 */
public interface CustomerService {
    Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName);

    ServiceVO saveCustomer(Customer customer);

    ServiceVO deleteCustomer(String ids);
}
