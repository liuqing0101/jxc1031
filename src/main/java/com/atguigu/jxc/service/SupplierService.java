package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/16 18:15
 * description:
 */
public interface SupplierService {
    Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName);

    ServiceVO saveSupplier(Supplier supplier);

    ServiceVO deleteSupplier(String ids);
}
