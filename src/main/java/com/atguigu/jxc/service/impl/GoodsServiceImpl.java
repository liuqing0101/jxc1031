package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerReturnListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.SaleListGoods;
import com.atguigu.jxc.service.GoodsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @description
 */
@Service
@SuppressWarnings("all")
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;
    @Autowired
    private SaleListGoodsDao saleListGoodsDao;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map=new HashMap<>();
        List<Goods> goodsList=goodsDao.listInventory((page-1)*rows, rows, codeOrName, goodsTypeId);

        //销售总量
        for (Goods goods : goodsList) {
            Integer saleTotal = saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId());
            Integer num=0;
            if (saleTotal!=null){
                 num=saleTotal;
                Integer returnTotal = customerReturnListGoodsDao.getCustomerReturnTotalByGoodsId(goods.getGoodsId());
                if (returnTotal!= null){
                    num=saleTotal-returnTotal;
                }
            }
            goods.setSaleTotal(num);
        }

        //分页信息
        map.put("total", goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map=new HashMap<>();
        List<Goods> goodsList=new ArrayList<>();
        List<Integer> typss = new ArrayList<>();
        if (goodsTypeId==null){
            goodsTypeId=1;
        }
        getAllGoodsTypeList(goodsTypeId,typss);
        List<Goods> list = new ArrayList<Goods>();
        for (Integer integer : typss) {
            list=goodsDao.listInventory((page-1)*rows, rows, codeOrName, integer);
            goodsList.addAll(list);
        }
        //分页信息
        map.put("rows",goodsList);
        map.put("total", goodsList.size());
        return map;

    }
    public void getAllGoodsTypeList(Integer goodsTypeId,List<Integer> typss) {

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(goodsTypeId);
        if (!CollectionUtils.isEmpty(goodsTypeList)) {
            for (GoodsType goodsType : goodsTypeList) {
                getAllGoodsTypeList(goodsType.getGoodsTypeId(), typss);
            }
        }
        typss.add(goodsTypeId);

    }


    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
    @Override
    public ServiceVO saveGoods(Goods goods) {
        //根据id判断goods是否存在
        if(goods.getGoodsId()==null){
            //根据名字判断，名字是否可用
            Goods exGoods=goodsDao.findGoodsByName(goods.getGoodsName());
            if(exGoods==null){
                goodsDao.saveGoods(goods);
            }else {
                return new ServiceVO<>(ErrorCode.ACCOUNT_EXIST_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);
            }
        }else {
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteGoods(Integer goodsId) {
        goodsDao.deleteGoods(goodsId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
         Map<String, Object> map= new HashMap<>();
         List<Goods> goodsList=goodsDao.getNoInventoryQuantity(page,rows,nameOrCode);
         map.put("total", goodsList.size());
         map.put("rows",goodsList);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map= new HashMap<>();
        List<Goods> goodsList=goodsDao.getHasInventoryQuantity(page,rows,nameOrCode);
        map.put("total", goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        goodsDao.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        boolean flag=goodsDao.deleteStock(goodsId);
        if (flag) {

            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }else{
            return new ServiceVO<>(ErrorCode.ACCOUNT_EXIST_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map=new HashMap<>();
        List<Goods> goodsList =goodsDao.listAlarm();
        map.put("rows",goodsList);
        return  map;
    }


}
