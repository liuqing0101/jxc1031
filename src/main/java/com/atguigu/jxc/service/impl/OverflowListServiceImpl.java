package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author liuqing
 * @date 2023/5/19 18:38
 * description:
 */
@Service
@SuppressWarnings("all")
public class OverflowListServiceImpl implements OverflowListService {
    @Autowired
    private OverflowListDao overflowListDao;

    @Override
    public ServiceVO saveOverflowList(HttpSession session,OverflowList overflowList, String overflowListGoodsStr) {
        List<OverflowListGoods> overflowListGoodsList = JSON.parseArray(overflowListGoodsStr, OverflowListGoods.class);
        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowList.setUserId(userId);
        overflowListDao.saveOverflowList(overflowList);
        overflowListGoodsList.stream().forEach(OverflowListGoods->{
            OverflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overflowListDao.saveOverflowListGoods(OverflowListGoods);
        });

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
