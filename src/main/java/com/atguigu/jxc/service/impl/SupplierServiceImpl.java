package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/16 18:15
 * description:
 */
@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    /**
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplierList(Integer page, Integer rows, String supplierName) {
         Map<String, Object>  map=new HashMap<>();

         List<Supplier> supplierList=supplierDao.getSupplierList((page-1)*rows, rows, supplierName);
         map.put("total",supplierList.size());
         map.put("rows",supplierList);
        return map;
    }

    /**
     * 添加供应商
     * @param supplier
     * @return
     */
    @Override
    public ServiceVO saveSupplier(Supplier supplier) {
        if(supplier.getSupplierId()==null){
            Supplier exSupplier=supplierDao.findSupplier(supplier.getSupplierName());
            if (exSupplier==null){
                supplierDao.addSupplier(supplier);
            }else {
                return new ServiceVO<>(ErrorCode.ACCOUNT_EXIST_CODE,ErrorCode.ACCOUNT_EXIST_MESS);
            }
        }else {
            supplierDao.updateSupplier(supplier);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteSupplier(String ids) {
        String[] split = ids.split(",");
        for (String id : split) {

            supplierDao.deleteSupplier(id);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
