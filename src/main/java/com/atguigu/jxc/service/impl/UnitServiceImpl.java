package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/17 11:45
 * description:
 */
@Service
public class UnitServiceImpl implements UnitService {
    @Autowired
    private UnitDao unitDao;

    @Override
    public Map<String, Object> getUnitList() {
         Map<String, Object> map=new HashMap<>();
        List<Unit> unitList = unitDao.getUnitList();
        map.put("rows",unitList);
        return map;
    }
}
