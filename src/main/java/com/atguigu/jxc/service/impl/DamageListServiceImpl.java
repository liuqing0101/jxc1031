package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/19 8:48
 * description:
 */
@Service
@SuppressWarnings("all")
public class DamageListServiceImpl implements DamageListService {
    @Autowired
    private DamageListDao damageListDao;



    @Override
    @Transactional
    public ServiceVO saveDamageListGoods(HttpSession session, DamageList damageList, String damageListGoodsStr) {
        //将JSON字符串转为JSON对象
        List<DamageListGoods> damageListGoodsList = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);

        User user = (User) session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        damageList.setUserId(userId);
        damageListDao.saveDamageList(damageList);
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageListDao.saveDamageListGoods(damageListGoods);
        }
        //&&逻辑与  具有短路功能   &是按位与  不具有短路  布尔值没有按位运算
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);

    }

    @Override
    public Map<String, Object> getDamageList(HttpSession session, String sTime, String eTime) {
         Map<String, Object> map=new HashMap<>();
        User user = (User) session.getAttribute("currentUser");
        String userName = user.getUserName();
        List<DamageList> damageLists=damageListDao.getDamageList(sTime,eTime);
        damageLists.stream().forEach(DamageList->{
            DamageList.setTrueName(userName);
        });
        map.put("rows",damageLists);
        return map;
    }
}
