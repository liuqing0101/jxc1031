package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuqing
 * @date 2023/5/16 21:28
 * description:
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;


    /**
     * 分页展示客户
     * @param page 当前页码
     * @param rows 每页显示记录数
     * @param customerName 客户名字
     * @return
     */
    @Override
    public Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName) {
         Map<String, Object> map=new HashMap<>();
         //调用dao层
         List<Customer> customerList=customerDao.getCustomerList((page-1)*rows, rows, customerName);
         map.put("total",customerList.size());
         map.put("rows",customerList);
        return map;
    }

    @Override
    public ServiceVO saveCustomer(Customer customer) {
        //判断客户是否存 不存在：添加  存在：修改
        if (customer.getCustomerId()==null){
            //判断用户名是否占用 占用返回一个错误提示
            Customer exCustomer=customerDao.findCustomer(customer.getCustomerName());
            if (exCustomer==null){
                customerDao.saveCustomer(customer);
            }else {
                return new ServiceVO<>(ErrorCode.ACCOUNT_EXIST_CODE,ErrorCode.ACCOUNT_EXIST_MESS);
            }
        }else{
            //修改
            customerDao.updateCustomer(customer);
        }

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteCustomer(String ids) {
        //将string转换为数组，条件是：","
        String[] split = ids.split(",");
        for (String id : split) {
            //遍历id，根据id删除
            customerDao.deleteCustomerById(id);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
