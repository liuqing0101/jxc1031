package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    GoodsType findGoodsType(String goodTypeName);

    void saveGoodsType(String goodsTypeName,  Integer pId);

    void deleteGoodsType(Integer goodsTypeId);

    GoodsType findGoodsTypeById(Integer goodsTypeId);
}
