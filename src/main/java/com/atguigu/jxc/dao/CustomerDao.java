package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;

import java.util.List;

/**
 * @author liuqing
 * @date 2023/5/16 21:28
 * description:
 */
public interface CustomerDao {
    List<Customer> getCustomerList(Integer page, Integer rows, String customerName);

    Customer findCustomer(String customerName);

    void saveCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomerById(String customerId);
}
