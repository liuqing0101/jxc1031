package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liuqing
 * @date 2023/5/16 18:22
 * description:
 */
public interface SupplierDao {
    List<Supplier> getSupplierList(@Param("page") Integer page, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    Supplier findSupplier(String supplierName);

    void addSupplier(Supplier supplier);

    void updateSupplier(Supplier supplier);

    void deleteSupplier(String supplierId);
}
