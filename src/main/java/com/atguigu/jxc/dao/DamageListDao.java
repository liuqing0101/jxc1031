package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

/**
 * @author liuqing
 * @date 2023/5/19 8:49
 * description:
 */
public interface DamageListDao {


    void saveDamageList(DamageList damageList);


    void saveDamageListGoods(DamageListGoods damageListGood);

    List<DamageList> getDamageList(String sTime, String eTime);
}
