package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

/**
 * @author liuqing
 * @date 2023/5/19 18:40
 * description:
 */
public interface OverflowListDao {
    void saveOverflowList(OverflowList overflowList);

    void saveOverflowListGoods(OverflowListGoods overflowListGoods);
}
